from math import *
from pathlib import Path
from multiprocessing import *

import shutil
import tempfile
import argparse
import sys
import time

class Engine:
    def __init__(self):
        self.maxdepth = 9
        self.mindisp = 0.0001

    def multiproc(self, scene, pcount, img):
        def sprange(c, p):
            div, rem = divmod(c, p)
            return [(j*div + min(j, rem), (j + 1) * div + min(j + 1, rem)) for j in range(p)]
        w, h = scene.w, scene.h
        rs = sprange(h, pcount)
        tdir = Path(tempfile.mkdtemp())
        tmpfile = "raytrace-process-{}.temp"
        proc = []
        try:
            rd = Value("i", 0)
            for minh, minmax in rs:
                pfile = tdir / tmpfile.format(minh)
                proc.append(Process(target = self.run, args = (scene, minh, minmax, pfile, rd)))
            for prc in proc:
                prc.start()
            for prc in proc:
                prc.join()
            Img.w_header(img, w = w, h = h)
            for minh, _ in rs:
                pfile = tdir / tmpfile.format(minh)
                img.write(open(pfile, "r").read())
        finally:
            shutil.rmtree(tdir)

    def run(self, s, minh, minm, pfile, rdone):
        w = s.w
        h = s.h
        aspect, x1, x0 = float(w) / h, +1.0, -1.0
        xs = (x1 - x0) / (w - 1)

        y1, y0 = +1.0 / aspect, -1.0 / aspect
        ys = (y1 - y0) / (h - 1)

        cam = s.cam
        p = Img(w, minm - minh)

        for k in range(minh, minm):
            y = y0 + k * ys
            for j in range(w):
                x = x0 + j * xs
                ray = Ray(cam, minusv([x, y, 0.0], cam))
                p.a_pix(j, k - minh, self.trace(ray, s))
            if rdone:
                with rdone.get_lock():
                    rdone.value += 1
                    print(f"Render progress: {round((rdone.value/h)*100, 3)}%", end="\r")
        with open(pfile, "w") as pf:
            p.w_ppm(pf)

    def trace(self, r, s, d = 0):
        clr = [0, 0, 0]
        dh, objh = self.find_n(r, s)
        if not objh:
            return clr
        hpos = addv(r.orig, multiplyv(r.dir, dh))
        hitn = objh.normal(hpos)
        clr = addv(clr, self.cat(objh, hpos, hitn, s))
        if d < self.maxdepth:
            new_pos = addv(hpos, multiplyv(hitn, self.mindisp))
            new_dir = minusv(r.dir, multiplyv(hitn, 2 * dotp(*r.dir, *hitn)))
            new_ray = Ray(new_pos, new_dir)
            tcl = multiplyv(self.trace(new_ray, s, d + 1), objh.m.ref)
            clr = addv(clr, tcl)
        return clr

    def find_n(self, r, s) -> tuple:
        dm, objh = None, None
        for obj in s.objs:
            d = obj.intr(r)
            if d != None and (objh == None or d < dm):
                dm = d
                objh = obj
        return dm, objh

    def cat(self, obh, hpos, hn, s):
        mat = obh.m
        objc = mat.cat(hpos)
        speck = 50
        tcam = minusv(s.cam, hpos)
        colr = multiplyv(cfhex("#000000"), mat.amb)
        for lg in s.lgs:
            tlg = Ray(hpos, minusv(lg.pos, hpos))
            toadd = multiplyv(objc, mat.diff)
            toadd = multiplyv(toadd, max(dotp(*hn, *tlg.dir), 0))
            colr = addv(colr, toadd)
            tv = addv(tlg.dir, tcam)
            hfv = divisionv(tv, magn(*tv))
            colr = addv(colr, multiplyv(multiplyv(lg.clr, mat.spec), max(dotp(*hn, *hfv), 0) ** speck))
        return colr

class Ray:
    def __init__(self, orig, dir):
        self.orig = orig
        self.dir = norm(dir, sqrt(dotp(*dir, *dir)))

class Scene:
    def __init__(self, cam, objs, lgs, w, h):
        self.cam = cam
        self.objs = objs
        self.lgs = lgs
        self.w = w
        self.h = h

# It's a ball
class Sphere:
    def __init__(self, c, r, m):
        self.c = c
        self.r = r
        self.m = m

    # Checks for possible intersect between ray and sphere
    def intr(self, ray):
        sptr = minusv(ray.orig, self.c)
        b = dotp(*ray.dir, *sptr) * 2
        c = dotp(*sptr, *sptr) - self.r ** 2
        disc = b * b - 4 * c
        if disc >= 0:
            dist = (-b - sqrt(disc)) / 2
            if dist > 0:
                return dist
        return None

    def normal(self, sp):
        mv = minusv(sp, self.c)
        return norm(mv, magn(*mv))

class Light:
    def __init__(self, pos, clr):
        self.clr = clr
        self.pos = pos

class Material:
    def __init__(self, clr, amb = 0.25, diff = 1.0, spec = 1, ref = 0.65):
        self.amb = amb
        self.spec = spec
        self.diff = diff
        self.clr = clr
        self.ref = ref

    def cat(self, pos):
        return self.clr

class ChessBoard:
    def __init__(self, clr, clr2, amb = 0.15, diff = 0.7, spec = 1, ref = 0.65):
        self.amb = amb
        self.spec = spec
        self.diff = diff
        self.clr = clr
        self.clr2 = clr2
        self.ref = ref

    def cat(self, pos):
        if int((pos[0] + 5.0) * 3.0) % 2 == int(pos[2] * 3.0) % 2:
            return self.clr
        return self.clr2

class Img:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        self.p = [[[0, 0, 0] for _ in range(self.w)] for _ in range(self.h)]

    def a_pix(self, x, y, c) -> None:
        def byt(l):
            return [round(max(min(j * 255, 255), 0)) for j in l]
        self.p[y][x] = byt(c)

    def w_ppm(self, img):
        self.w_file(img)

    @staticmethod
    def w_header(img, w = None, h = None):
        img.write(f"P3\n{w} {h}\n255\n")

    def w_file(self, img) -> None:
        for r in self.p:
            for c in r:
                img.write(f" {c[0]}  {c[1]}  {c[2]}   ")
            img.write("\n")
            #print(f"\033[92mRendering done!\033[0m Write progress: {round((self.count/self.total)*100, 3)}%", end="\r")

#Color to PPM byte "vector"
def bytev(v) -> list:
    return [round(max(min(x * 255, 255), 0)) for x in v]

#Returns given vector magnitude
def magn(x, y, z) -> int:
    return sqrt(x**2 + y**2 + z**2)

#Returns normalized vector
def norm(v, mv) -> list:
    return [x / mv for x in v]

#Returns dot product of given vectors
def dotp(x, y, z, xs, ys, zs) -> int:
    return x*xs + y*ys + z*zs

#Returns sum vector of 2 given vectors
def addv(v, vs) -> list:
    return [j + k for j, k in zip(v, vs)]

#Returns substracted vector of 2 given vectors
def minusv(v, vs) -> list:
    return [j - k for j, k in zip(v, vs)]

#Returns multiplied vector of given vector and integer value T
def multiplyv(v, t) -> list:
    return [j * t for j in v]

#Returns divided vector of given vector and integer value T
def divisionv(v, t) -> list:
    if t == 0: raise ValueError("Division with 0!")
    return [j/t for j in v]

def aspr(w, h) -> tuple:
    asp = w/h
    return 1/asp, -(1/asp)

def cfhex(h) -> list:
    return [int(h[x:x+2], 16) / 255.0 for x in [1, 3, 5]]

def assertEquals(test, real) -> bool:
    return True if test == real else False

def run():
    argparser = argparse.ArgumentParser()

    argparser.add_argument("-j", action = "store", type = int, dest = "procs", default = 0, help = "No. of processes")
    argparser.add_argument("-n", action = "store", type = str, dest = "fname", default = "render", help = "Name of the output file")
    argparser.add_argument("-x", action = "store", type = int, dest = "width", default = 1440, help = "Width of the picture")
    argparser.add_argument("-y", action = "store", type = int, dest = "height", default = 900, help = "Height of the picture")
    argparser.add_argument("-m", dest = "math", action = "store_true", help = "Test math functions")
    argparser.set_defaults(math = False)

    args = argparser.parse_args()
    pcount = cpu_count() if args.procs == 0 else args.procs
    if args.math: testmath()

    startime = time.time()
    w, h, defaultn = args.width, args.height, args.fname
    cam = [0, -0.35, -1.15]
    objs = [
            Sphere([0, 10000.5, 1], 10000.0, ChessBoard(clr = cfhex("#ffffff"), clr2 = cfhex("#000000"))),
            Sphere([0.75, -0.125, 2.25], 0.6, Material(cfhex("#349199"), amb = 0.04, ref = 0.45, diff = 0.45)),
            Sphere([-0.75, -0.15, 1], 0.65, Material(cfhex("#a60372"), amb = 0.06, diff = 0.92, ref = 0.65)),
            Sphere([0.985, 0.12, 1.25], 0.35, Material(cfhex("#82D11F")))
           ]
    lights = [
              Light([2.5, 2.5, -8], cfhex("#d7ab6f")),
              Light([-1.5, -11.5, 1], cfhex("#ffffff"))
             ]
    sc = Scene(cam, objs, lights, w, h)
    re = Engine()

    with open(f"{defaultn}.ppm", "w") as imgf:
        re.multiproc(sc, pcount, imgf)

    totaltime = time.time() - startime
    print(f"\n\033[92mDone!\033[0m\nOutput file: {defaultn}.ppm\n\033[94mTime took: {round(totaltime, 3)}s with {pcount} cores")

# Check if current math helper functions are working properly
def testmath():
    v1, v2, w, h = [-2, 1, 5], [2, -1, -5], 1440, 900
    tests = [
        str(round(magn(*v1), 3)), "5.477",
        str(norm(v1, magn(*v1))), "[-0.365, 0.183, 0.913]",
        str(dotp(*v1, *v2)), "-30",
        str(addv(v1, v2)), "[0, 0, 0]",
        str(minusv(v1, v2)), "[-4, 2, 10]",
        str(multiplyv(v1, 2)), "[-4, 2, 10]",
        str([round(j, 3) for j in divisionv(v2, 3)]), "[0.667, -0.333, -1.667]",
        str(aspr(w, h)), "(0.625, -0.625)",
        str(cfhex("#FF0000")), "[1.0, 0.0, 0.0]"
        ]

    for i in range(0, len(tests), 2):
        k, v = tests[i], tests[i+1]
        if not assertEquals(k, v):
            print(f"\033[91mTest failed!\033[0m {k} != {v}")
        else:
            print(f"\033[92mTest passed!\033[0m {k} == {v}")
    sys.exit(0)

if __name__ == "__main__":
    run()
